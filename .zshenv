# Zsh config sources
export ANTIDOTE_HOME=~/.cache/antidote
export ZDOTDIR=$HOME/.config/zsh
source $ZDOTDIR/.zshrc

# XDG
export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache
export XDG_DATA_HOME=$HOME/.local/share
export XDG_STATE_HOME=$HOME/.local/state"
export XDG_SCREENSHOTS_DIR=$HOME/Pictures/Screenshots

# Always use Unicode line-drawing characters, not VT100-style ones
export NCURSES_NO_UTF8_ACS
zeNCURSES_NO_UTF8_ACS=1

# editor 
export EDITOR=nvim
export VISUAL=nvim
export VIMCONFIG=$XDG_CONFIG_HOME/nvim
export MYVIMRC=$XDG_CONFIG_HOME/nvim/init.lua

# Customization
export QT_DEBUG_PLUGINS=0
export GSETTINGS_SCHEMA_DIR=~/.config/gtk-3.0/settings.ini
export XCURSOR_SIZE=32
export XCURSOR_THEME=ArchCursorComplete
export HYPRCURSOR_SIZE=32
export HYPRCURSOR_THEME=ArchCursorComplete
export GTK_THEME=Dracula
export ICON_THEME=Papirus-Dark
gsettings set org.gnome.desktop.interface cursor-theme $XCURSOR_THEME
gsettings set org.gnome.desktop.interface cursor-size $XCURSOR_SIZE
gsettings set org.gnome.desktop.interface gtk-theme $GTK_THEME
gsettings set org.gnome.desktop.wm.preferences theme $GTK_THEME
gsettings set org.gnome.desktop.interface icon-theme $ICON_THEME

# Firefox
export MOZ_DBUS_REMOTE=1
export GDK_BACKEND=wayland
export MOZ_ENABLE_WAYLAND=1

# pager
export PAGER='less -R'
export MANPAGER='nvim +Man!'
export MANWIDTH=999

# fzf 
export FZF_DEFAULT_COMMAND="rg --files --hidden --glob '!.git':
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_COLORS="\
--color=fg:#f8f8f2,bg:#282a36,hl:#bd93f9 \
--color=fg+:#f8f8f2,bg+:#44475a,hl+:#bd93f9 \
--color=info:#ffb86c,prompt:#50fa7b,pointer:#ff79c6 \
--color=marker:#ff79c6,spinner:#ffb86c,header:#6272a4"
export FZF_DEFAULT_OPTS="--height 40% \
--border sharp \
--layout reverse \
--color '$FZF_COLORS' \
--prompt '∷ ' \
--pointer ▶ \
--marker ⇒" 
export FZF_ALT_C_OPTS="--preview 'tree -C {} | head -n 10'" 
export FZF_COMPLETION_DIR_COMMANDS="cd pushd rmdir tree eza"

# eza universal Dracula
export EZA_COLORS="\
uu=36:\
uR=31:\
un=35:\
gu=37:\
da=2;34:\
ur=34:\
uw=95:\
ux=36:\
ue=36:\
gr=34:\
gw=35:\
gx=36:\
tr=34:\
tw=35:\
tx=36:\
xx=95:"

export FZF_DEFAULT_OPTS='\
--color=fg:#f8f8f2,bg:#282a36,hl:#bd93f9 \
--color=fg+:#f8f8f2,bg+:#44475a,hl+:#bd93f9 \
--color=info:#ffb86c,prompt:#50fa7b,pointer:#ff79c6 \
--color=marker:#ff79c6,spinner:#ffb86c,header:#6272a4'

# history
export HISTFILE=$XDG_CACHE_HOME/zsh/.zhistory
export HISTSIZE=10000
export SAVEHIST=12000
  
# You can use .zprofile to set environment vars for non-login, non-interactive shells. 
if [[ ( "$SHLVL" -eq 1 && ! -o LOGIN ) && -s "${ZDOTDIR:-$HOME}/.zprofile" ]]; then 
  source "${ZDOTDIR:-$HOME}/.zprofile" 
fi



#vim:ft=zsh
