" vim-hyprland-nav.vim -- Use hyprland's focus direction bindings to move between vim
" splits as well. Requires the accompanying helper script.
"
" Inspired by https://git.sr.ht/~jcc/vim-sway-nav

let clientserver = has("nvim") || has("clientserver")
if exists("g:loaded_vim_hyprland_nav") || !clientserver
    finish
endif
let g:loaded_vim_hyprland_nav = 1

function s:get_method()
    let l:method = 'file'
    if has_key(environ(), 'VIM_HYPRLAND_NAV_METHOD')
        let l:method = $VIM_HYPRLAND_NAV_METHOD
    endif
    return l:method
endfunction

function s:setup()
    " Ensure we are running a server.
    if empty(v:servername) && !has("nvim")
        call remote_startserver(rand())
    endif


    let l:program = has("nvim") ? "nvim" : "vim"
    let l:identifier = l:program . " " . v:servername

    let l:method = s:get_method()

    if l:method ==? 'file'
        " Create a file so the helper script knows how to send a command.
        let runtime_dir = empty($XDG_RUNTIME_DIR) ? "/tmp" : $XDG_RUNTIME_DIR
        let s:servername_file = runtime_dir . "/vim-hyprland-nav." . getpid() . ".servername"
        call writefile([l:identifier], s:servername_file)
    elseif l:method ==? 'title'
        set title
        let l:title=l:identifier
        exec 'set titlestring=vim-hyprland-nav\ ' . escape(l:title, ' ')
    else
        echoerr 'Invalid vim-hyprland-nav#method: ' . l:method
    endif
endfunction

function s:cleanup()
    if s:get_method() ==? 'file'
        call delete(s:servername_file)
    endif
endfunction

" Schedule setup and cleanup.
augroup vim_hyprland_nav
    autocmd!
    autocmd VimEnter * call s:setup()
    autocmd VimLeavePre * call s:cleanup()
augroup END

" Do some shenanigans to be compatible with jobs in vim and nvim (jobs are
" used instead of system() to avoid starting a shell and running potentially
" slow shell initialization files).
if has("nvim")
    function! s:job(cmd)
        return jobstart(a:cmd)
    endfunction
else
    function! s:job(cmd)
        return job_start(a:cmd)
    endfunction
endif

" Function to be called remotely by the helper script.
function VimHyprlandNav(dir)
    let l:dir_flag = get({"left": "h", "down": "j", "up": "k", "right": "l"}, a:dir)
    if winnr(l:dir_flag) == winnr()
        call s:job(["hyprctl", "dispatch", "movefocus", strcharpart(a:dir, 0, 1)])
    else
        execute "wincmd " . l:dir_flag
    endif
endfunction
