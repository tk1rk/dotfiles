return {
    'konapun/vacuumline.nvim', 
    branch = 'next', 
    dependencies = {
        'glepnir/galaxyline.nvim', branch = 'main',
        'kyazdani42/nvim-web-devicons', 
    }, 
    config = { 
        require('vacuumline').setup({
            theme = require('vacuumline.theme.nord')
        }) 
    }
}