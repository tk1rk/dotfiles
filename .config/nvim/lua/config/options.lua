local opts = {
	autowrite = true,
    clipboard = "unnamedplus",         
    completeopt = [ "menuone", "noinsert", "noselect" ], -- Autocomplete options 
    grepformat = "%f:%l:%c:%m", 
    grepprg = "rg --vimgrep", 
    laststatus = 0, 
    list = true,                       -- Show some invisible characters (tabs... 
    listchars = {eol = '↲', tab = '▸ ', trail = '·'},
    mouse = "a",                       -- Enable mouse mode 
    number = true,                     -- Show line number 
    numberwidth = 2,                   -- minimal number of columns to use for the line number {default 4}
    pumblend = 10,                     -- Popup blend 
    pumheight = 10,                    -- Maximum number of entries in a popup 
    relativenumber = false,            -- 
    ruler = false,                     -- hide the line and column number of the cursor position
    sessionoptions = { "buffers", "curdir", "tabpages", "winsize" }, 
    showcmd = false,                   -- hide (partial) command in the last line of the screen (for performance)
    signcolumn = "yes",                -- always show the sign column, otherwise it would shift the text each time
    swapfile = false,                  -- Don't use swapfile 
    showmode = false,                   -- Dont show mode since we have a statusline
    scrolloff = 8,                     -- minimal number of screen lines to keep above and below the cursor
    sidescrolloff = 8,                 -- minimal number of screen columns to keep to the left and right of the cursor if wrap is `false`
    wrap = false,                      -- display lines as one long line

    -- Neovim UI 
    showmatch = true,        -- Highlight matching parenthesis 
    foldmethod = "syntax",   -- Enable folding (default 'foldmarker') 
    splitright = true,       -- Vertical split to the right 
    splitbelow = true,       -- Horizontal split to the bottom 
    linebreak = true,        -- Wrap on word boundary 
    termguicolors = true,    -- Enable 24-bit RGB colors 
    t_Co=256
    laststatus = 3,            -- Set global statusline 
    cmdheight = 0,       
    wildmode = "longest:full,full", -- Command-line completion mode
    wildmenu = true,

    -- search
    ignorecase = true,       -- Ignore case 
    inccommand = "nosplit",  -- preview incremental substitute  
    smartcase = true,        -- Ignore lowercase for the whole pattern 

    -- Tabs, indent 
    expandtab = true,        -- Use spaces instead of tabs 
    shiftwidth = 4,          -- Shift 4 spaces when tab 
    tabstop = 4,             -- 1 tab == 4 spaces 
    smartindent = true,      -- Auytoindent new lines 
    softtabstop = 4,  
    shiftround = true,       -- Round indent 
 
    -- Memory, CPU 
    hidden = true,           -- Enable background buffers 
    history = 100,           -- Remember N lines in history 
    lazyredraw = true,       -- Faster scrolling 
    synmaxcol = 240,         -- Max column for syntax highlight 
    updatetime = 250,        -- ms to wait for trigger an event 
 
}

-- Set options from table
for opt, val in pairs(opts) do
	vim.o[opt] = val
end

-- Set other options
local colorscheme = require("helpers.colorscheme")
vim.cmd.colorscheme(colorscheme)
